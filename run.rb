#!/usr/bin/env ruby

require 'tmpdir'
require 'yaml'

class Environment
    attr_accessor :title, :description

    def initialize(yaml)
        @title = yaml['Title']
        @description = yaml['Description']
        @run = yaml['Run']
    end

    def self.new(yaml)
        if yaml['Environment'] == 'vanilla'
          object = Vanilla.allocate
        elsif yaml['Environment'] == 'CDE'
          object = CDE.allocate
        end
        object.send :initialize, yaml
        object
    end

    def run
        raise "not implemented for generic environment"
    end
end

class Vanilla < Environment
    def run
        puts "running Vanilla experiment..."
        system(@run)
    end
end

class CDE < Environment
    def run
        puts "running CDE experiment..."
        system(File.join("cde-package", @run + ".cde"))
    end
end

if ARGV.length < 1
    puts "Usage: #{$0} <experiment repository>"
    exit
end

repo = ARGV[0]

Dir.mktmpdir("expack") { |dir|
    system("git clone #{repo} #{dir}")
    Dir.chdir(dir)
    exfile = YAML.load_file("main.expack")
    env = Environment.new(exfile)
    env.run
}
